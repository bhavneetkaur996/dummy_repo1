/**
Description     : This is Scheduler Job for performing Merging. 
					i.e, it takes unprocessed records from and process them using a batch process.
Created By      : Kishore Meesala
Created Date    : oct 17, 2018
***********************************************
Modification Log: 
-------------------------------------------------------------------
Modified_By			Modified_datetime	User_Story		Desc_of_modifications
-------------------------------------------------------------------

* */
global class CPS_Batch_Trigger_Table_SchedulerTask implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        System.debug('INFO START // CPS_Batch_Trigger_Table_SchedulerTask.execute');
        CPS_BatchApexMergeHelper.performExternalBatchJob(CPS_Constants.BATCH_NAME_MERGE);
        System.debug('INFO END // CPS_Batch_Trigger_Table_SchedulerTask.execute');
    }
}